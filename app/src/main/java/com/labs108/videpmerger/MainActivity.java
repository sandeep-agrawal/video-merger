package com.labs108.videpmerger;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Container;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public List<String> files = new ArrayList<>();
    public double firstlength;
    public double secondlength;
    public double firstinterval = 10;
    public double secondinterval = 10;
    public List<String> filesname = new ArrayList<>();
    public double start = 0;
    public double startsecond =0;
    public ProgressDialog progressDialog;
    public TextView first;
    public TextView second;
    public SeekBar firstSeekbar;
    public SeekBar secondSeekbar;
    public TextView split;
    public int count = 0;
    List<List<Track>> movies = new ArrayList<>();

    List<Track> videoTracks = new LinkedList<>();
    List<Track> audioTracks = new LinkedList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        first = (TextView)findViewById(R.id.textView1);
        second = (TextView)findViewById(R.id.textView2);
        firstSeekbar = (SeekBar)findViewById(R.id.seekBar1);
        secondSeekbar = (SeekBar)findViewById(R.id.seekBar2);
        split = (TextView)findViewById(R.id.split) ;

        firstSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean b) {
                progress =  progressValue;
                if(progressValue == 0) {
                    firstinterval = 10;
                } else {
                    firstinterval = progressValue;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                first.setText("Split Interval: " + progress + "/" + seekBar.getMax());
            }
        });

        secondSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean b) {
                progress =  progressValue;
                if(progressValue == 0) {
                    secondinterval = 10;
                } else {
                    secondinterval = progressValue;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                second.setText("Split Interval: " + progress + "/" + seekBar.getMax());
            }
        });


        split.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                filesname.clear();
                movies.clear();
                videoTracks.clear();
                audioTracks.clear();
                progressDialog = new ProgressDialog(MainActivity.this);
                progressDialog.setMessage("Spliting video clips");
                progressDialog.setCancelable(false);
                progressDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //append();
                        Log.e("Values",""+firstlength+secondlength+firstinterval+secondinterval);
                        try {
                            while(firstlength > 0 || secondlength >0) {

                                if(firstlength > 0) {
                                    if(firstlength > firstinterval) {
                                        final File source = new File(files.get(0));
                                        String filename = String.format(Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+System.currentTimeMillis()+".mp4");
                                        filesname.add(filename);
                                        final File output = new File(filename);
                                        startTrim(source, output, start, start + firstinterval);
                                        start = start+firstinterval+.7;
                                        firstlength = firstlength-firstinterval;
                                        Log.e("filename1 in 10",""+filename);
                                    } else {
                                        final File source = new File(files.get(0));
                                        String filename = String.format(Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+System.currentTimeMillis()+".mp4");
                                        filesname.add(filename);
                                        final File output = new File(filename);
                                        startTrim(source, output, start, start + firstlength);
                                        start = 0;
                                        firstlength = 0;
                                        Log.e("filename1",""+filename);
                                    }
                                }

                                if(secondlength > 0) {
                                    if(secondlength > secondinterval) {
                                        final File source = new File(files.get(1));
                                        String filename = String.format(Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+System.currentTimeMillis()+".mp4");
                                        filesname.add(filename);
                                        final File output = new File(filename);
                                        startTrim(source, output, startsecond, startsecond+secondinterval);
                                        startsecond = startsecond+secondinterval+.7;
                                        secondlength = secondlength-secondinterval;
                                        Log.e("filename2 in 10",""+filename);
                                    } else {
                                        final File source = new File(files.get(1));
                                        String filename = String.format(Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+System.currentTimeMillis()+".mp4");
                                        filesname.add(filename);
                                        final File output = new File(filename);
                                        startTrim(source, output, startsecond, startsecond + secondlength);
                                        startsecond = 0;
                                        secondlength = 0;
                                        Log.e("filename2",""+filename);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.e("Exception",""+ e.getMessage());
                        }

                        append();
                    }
                }).start();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getAllVideo();
    }

    private void getAllVideo(){
        File file1 = new File(String.format(Environment.getExternalStorageDirectory().getAbsolutePath()+"/output.mp4"));
        if(file1.exists()) {
            file1.delete();
        }

        movies.clear();
        videoTracks.clear();
        audioTracks.clear();
        // String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        String[] projection = {
                MediaStore.Video.Media.TITLE,
                MediaStore.Video.Media.DURATION,
                MediaStore.Video.Media.DATA,
                MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.SIZE,
                MediaStore.Video.Media.DATE_MODIFIED

        };

        Cursor cursor = null;
        try {
            Log.e("video","in video");
            Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            cursor = this.getContentResolver().query(uri, projection, null, null, MediaStore.Video.Media.DATE_MODIFIED+ " DESC");
            if( cursor != null){
                //cursor.moveToFirst();
                while( cursor.moveToNext() && count<2 ){
                    count++;
                    Log.e("video",""+cursor.getString(2));
                    //System.out.print("in video" +cursor.getString(2));
                    files.add(cursor.getString(2));
                }
                count = 0;
            }

            IsoFile isoFile = new IsoFile(files.get(0));
            firstlength = (double)
                    isoFile.getMovieBox().getMovieHeaderBox().getDuration() /
                    isoFile.getMovieBox().getMovieHeaderBox().getTimescale();
            Log.e("firstlength",""+firstlength);
            firstSeekbar.setMax((int)firstlength);
            first.setText("Split Interval: " + firstSeekbar.getProgress() + "/" + firstSeekbar.getMax());
            isoFile = new IsoFile(files.get(1));
            secondlength = (double)
                    isoFile.getMovieBox().getMovieHeaderBox().getDuration() /
                    isoFile.getMovieBox().getMovieHeaderBox().getTimescale();
            Log.e("secondlength",""+secondlength);
            secondSeekbar.setMax((int)secondlength);
            second.setText("Split Interval: " + secondSeekbar.getProgress() + "/" + secondSeekbar.getMax());
            // append();

        } catch (Exception e) {
            Log.e("Excdption in video","Excdption in video");
            if(progressDialog != null) {
                progressDialog.dismiss();
            }
        }finally{
            if( cursor != null){
                cursor.close();
            }
        }
    }


    private static double correctTimeToSyncSample(Track track, double cutHere, boolean next) {
        double[] timeOfSyncSamples = new double[track.getSyncSamples().length];
        long currentSample = 0;
        double currentTime = 0;
        for (int i = 0; i < track.getSampleDurations().length; i++) {
            long delta = track.getSampleDurations()[i];

            if (Arrays.binarySearch(track.getSyncSamples(), currentSample + 1) >= 0) {
                timeOfSyncSamples[Arrays.binarySearch(track.getSyncSamples(), currentSample + 1)] = currentTime;
            }
            currentTime += (double) delta / (double) track.getTrackMetaData().getTimescale();
            currentSample++;

        }
        double previous = 0;
        for (double timeOfSyncSample : timeOfSyncSamples) {
            if (timeOfSyncSample > cutHere) {
                if (next) {
                    return timeOfSyncSample;
                } else {
                    return previous;
                }
            }
            previous = timeOfSyncSample;
        }
        return timeOfSyncSamples[timeOfSyncSamples.length - 1];
    }

    public  void startTrim(File src, File dst, double startMs, double endMs) throws IOException {
        FileDataSourceImpl file = new FileDataSourceImpl(src);
        Movie movie = MovieCreator.build(file);
        Log.e("time track",""+movie.getTimescale());
        // remove all tracks we will create new tracks from the old
        List<Track> tracks = movie.getTracks();
        movie.setTracks(new LinkedList<Track>());
        double startTime = startMs;
        double endTime = endMs;
        boolean timeCorrected = false;
        // Here we try to find a track that has sync samples. Since we can only start decoding
        // at such a sample we SHOULD make sure that the start of the new fragment is exactly
        // such a frame
        for (Track track : tracks) {
            if (track.getSyncSamples() != null && track.getSyncSamples().length > 0) {
                if (timeCorrected) {
                    // This exception here could be a false positive in case we have multiple tracks
                    // with sync samples at exactly the same positions. E.g. a single movie containing
                    // multiple qualities of the same video (Microsoft Smooth Streaming file)
                    throw new RuntimeException("The startTime has already been corrected by another track with SyncSample. Not Supported.");
                }
                startTime = correctTimeToSyncSample(track, startTime, false);
                endTime = correctTimeToSyncSample(track, endTime, true);
                timeCorrected = true;
            }
        }
        for (Track track : tracks) {
            long currentSample = 0;
            double currentTime = 0;
            long startSample = -1;
            long endSample = -1;

            for (int i = 0; i < track.getSampleDurations().length; i++) {
                if (currentTime <= startTime) {

                    // current sample is still before the new starttime
                    startSample = currentSample;
                }
                if (currentTime <= endTime) {
                    // current sample is after the new start time and still before the new endtime
                    endSample = currentSample;
                } else {
                    // current sample is after the end of the cropped video
                    break;
                }
                currentTime += (double) track.getSampleDurations()[i] / (double) track.getTrackMetaData().getTimescale();
                currentSample++;
            }
            movie.addTrack(new CroppedTrack(track, startSample, endSample));
        }
        movies.add(movie.getTracks());
//        Container out = new DefaultMp4Builder().build(movie);
//        FileOutputStream fos = new FileOutputStream(dst);
//        WritableByteChannel fc = fos.getChannel();
        try {
           // out.writeContainer(fc);
        } finally {
           // fc.close();
            //fos.close();
           // file.close();
        }

        //file.close();
    }
    public void append(){
        Log.e("in append","in append");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.setMessage("Merging video clips");
            }
        });

        try {


//            for( int i = 0; i<filesname.size();i++) {
//                movies.add(MovieCreator.build(filesname.get(i)).getTracks());
//            }
            final Movie finalMovie = new Movie();
            for ( int j =0 ; j< movies.size(); j++) {
                List<Track> tracks = movies.get(j);

                for ( int m =0 ;m < tracks.size() ; m++) {
                    Track track = tracks.get(m);

                    if(track.getHandler().equals("vide")) {
                        videoTracks.add(track);

                    } else if (track.getHandler().equals("soun")) {
                        audioTracks.add(track);
                    }
                }

            }
            if (audioTracks.size() > 0) {
                finalMovie.addTrack(new AppendTrack(audioTracks.toArray(new Track[audioTracks.size()])));
            }
            if (videoTracks.size() > 0) {
                finalMovie.addTrack(new AppendTrack(videoTracks.toArray(new Track[videoTracks.size()])));
            }

            final Container container = new DefaultMp4Builder().build(finalMovie);



            FileChannel fc = new RandomAccessFile(String.format(Environment.getExternalStorageDirectory().getAbsolutePath()+"/output.mp4"), "rw").getChannel();
            container.writeContainer(fc);
            fc.close();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    for( int i =0 ; i< filesname.size() ; i++) {
                        File file = new File(filesname.get(i));
                        if(file.exists()) {
                            file.delete();
                        }
                    }
                }
            }).start();


            if(progressDialog != null) {
                progressDialog.dismiss();
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(Environment.getExternalStorageDirectory().getAbsolutePath()+"/output.mp4")));
            intent.setDataAndType(Uri.parse(String.format(Environment.getExternalStorageDirectory().getAbsolutePath()+"/output.mp4")), "video/*");
            startActivity(intent);
        } catch (Exception e) {
            Log.e(" exception in append",""+e.getMessage());
        }
    }
}
